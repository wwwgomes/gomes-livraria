import React, { Component } from 'react';

const TableHead = () => {
  return (
    <thead>
      <tr>
        <th>Autores</th>
        <th>Livro</th>
        <th>Preços</th>
        <th>Remover</th>
      </tr>
    </thead>
  )
}

const TableBody = ({ autores, removeAutor }) => {
  return (
    <tbody>
      {autores.map((autor, index) => (
        <tr key={index}>
          <td>{ autor.nome }</td>
          <td>{ autor.livro }</td>
          <td>{ autor.preco }</td>
          <td><button className="waves-effect waves-light indigo lighten-2 btn" onClick={() => removeAutor(index)}>Remover</button></td>
        </tr>
      ))}
    </tbody>
  );
}

class Table extends Component {
  render() {
    const { autores, removeAutor } = this.props;

    return (
      <table className="centered highlight">
        <TableHead />
        <TableBody autores={ autores } removeAutor={ removeAutor } />
      </table>
    );
  }
}

export default Table;
